# Suppress Unknown Item

Suppress that log-spammy "Unknown item" error when loading recipes for items that don't exist.

## Information

Check out this mod on [CurseForge][].

## Building from source

```bash
git clone https://gitlab.com/sargunv-mc-mods/suppress-unknown-item.git
cd suppress-unknown-item
./gradlew build
# On Windows, use "gradlew.bat" instead of "gradlew"
```

[CurseForge]: https://minecraft.curseforge.com/projects/suppress-unknown-item
