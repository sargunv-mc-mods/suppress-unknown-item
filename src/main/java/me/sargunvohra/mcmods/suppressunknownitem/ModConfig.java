package me.sargunvohra.mcmods.suppressunknownitem;

import me.sargunvohra.mcmods.autoconfig1u.ConfigData;
import me.sargunvohra.mcmods.autoconfig1u.annotation.Config;
import me.sargunvohra.mcmods.autoconfig1u.annotation.ConfigEntry;
import org.apache.logging.log4j.Level;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

@SuppressWarnings("FieldMayBeFinal")
@Config(name = "suppressunknownitem")
@Config.Gui.Background("minecraft:textures/block/obsidian.png")
public class ModConfig implements ConfigData {

    private boolean enabled = true;

    private LogLevel logLevel = LogLevel.DEBUG;

    @ConfigEntry.Gui.Excluded
    private List<String> recipeMessageMatchers = Arrays.asList(
        "^Unknown item .+",
        "^Invalid or unsupported recipe type .+"
    );

    @Override
    public void validatePostLoad() throws ValidationException {
        try {
            Objects.requireNonNull(logLevel);
            Objects.requireNonNull(recipeMessageMatchers);
            for (String r : recipeMessageMatchers) {
                Objects.requireNonNull(r);
                Pattern.compile(r);
            }
        } catch (Exception e) {
            throw new ValidationException(e);
        }
    }

    public boolean getEnabled() {
        return enabled;
    }

    public LogLevel getLogLevel() {
        return logLevel;
    }

    public List<String> getRecipeMessageMatchers() {
        return recipeMessageMatchers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModConfig modConfig = (ModConfig) o;
        return enabled == modConfig.enabled &&
            logLevel.equals(modConfig.logLevel) &&
            recipeMessageMatchers.equals(modConfig.recipeMessageMatchers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(enabled, logLevel, recipeMessageMatchers);
    }

    @Override
    public String toString() {
        return "ModConfig{" +
            "enabled=" + enabled +
            ", logLevel=" + logLevel +
            ", recipeMessageMatchers=" + recipeMessageMatchers +
            '}';
    }

    @SuppressWarnings("unused")
    public enum LogLevel {
        ERROR(Level.ERROR),
        WARN(Level.WARN),
        INFO(Level.INFO),
        DEBUG(Level.DEBUG);

        private Level level;

        LogLevel(Level level) {
            this.level = level;
        }

        public Level getLog4jLevel() {
            return level;
        }
    }
}
