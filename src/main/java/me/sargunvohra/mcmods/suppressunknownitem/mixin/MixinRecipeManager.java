package me.sargunvohra.mcmods.suppressunknownitem.mixin;

import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import me.sargunvohra.mcmods.suppressunknownitem.ModConfig;
import net.minecraft.recipe.RecipeManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Mixin(RecipeManager.class)
public class MixinRecipeManager {
    private List<Pattern> patterns;

    @Inject(method = "apply", at = @At("HEAD"))
    private void prepareRegex(CallbackInfo ci) {
        patterns = AutoConfig.getConfigHolder(ModConfig.class).getConfig()
            .getRecipeMessageMatchers()
            .stream()
            .map(Pattern::compile)
            .collect(Collectors.toList());
    }

    @Redirect(
        method = "apply",
        at = @At(
            value = "INVOKE",
            target = "Lorg/apache/logging/log4j/Logger;error(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V",
            remap = false
        )
    )
    private void suppress(Logger logger, String message, Object id, Object exception) {
        ModConfig config = AutoConfig.getConfigHolder(ModConfig.class).getConfig();
        if (config.getEnabled()) {
            String exceptionMessage = ((Exception) exception).getMessage();
            Level level = config.getLogLevel().getLog4jLevel();

            for (Pattern p : patterns) {
                if (p.matcher(exceptionMessage).matches()) {
                    logger.log(level, message + " due to: {}", id, exceptionMessage);
                    return;
                }
            }
        }
        logger.error(message, id, exception);
    }
}
