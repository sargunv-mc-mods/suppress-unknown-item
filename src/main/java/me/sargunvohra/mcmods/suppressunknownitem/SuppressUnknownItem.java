package me.sargunvohra.mcmods.suppressunknownitem;

import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import me.sargunvohra.mcmods.autoconfig1u.serializer.Toml4jConfigSerializer;
import net.fabricmc.api.ModInitializer;

@SuppressWarnings("unused") // entrypoint
public class SuppressUnknownItem implements ModInitializer {

    @Override
    public void onInitialize() {
        AutoConfig.register(ModConfig.class, Toml4jConfigSerializer::new);
    }
}
